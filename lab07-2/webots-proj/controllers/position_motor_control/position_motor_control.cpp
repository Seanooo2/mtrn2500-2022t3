// Replace Webot's provided main file with this because the former contains a lot of bad style.

#include <iostream>

// Remember to #include these files.
#include <webots/Motor.hpp>
#include <webots/Robot.hpp>

constexpr int duration{64};  // Robot cycle duration.

void wait(webots::Robot& robot, double waitPeriod) {
    const double start{robot.getTime()};
    while (robot.getTime() - start < waitPeriod * 0.001) {
        robot.step(duration);
    }
}

void halt(webots::Motor& leftMotor, webots::Motor& rightMotor);

void moveForward(webots::Motor& leftMotor, webots::Motor& rightMotor);

// void moveForward(webots::Motor& leftMotor, webots::Motor& rightMotor,
//                  webots::PositionSensor& leftSensor, webots::PositionSensor& rightSensor);

// void moveBackward(webots::Motor& leftMotor, webots::Motor& rightMotor,
//                   webots::PositionSensor& leftSensor, webots::PositionSensor& rightSensor);

// void turnRight(webots::Motor& leftMotor, webots::Motor& rightMotor,
//                webots::PositionSensor& leftSensor, webots::PositionSensor& rightSensor);

int main() {
    webots::Robot controller;
    webots::Motor& leftMotor{*controller.getMotor("left wheel motor")};
    webots::Motor& rightMotor{*controller.getMotor("right wheel motor")};

    halt(leftMotor, rightMotor);

    // DO STUFF HERE.
}
